# traefik.server

## Для запуска

```
export = $PATH=$PATH:$PWD/bin
proxy_start
```

или 

```
cd /bin && ./proxy_start
```

## Для перезапуска

```
proxy_restart
```

или

```
cd /bin && ./proxy_restart
```